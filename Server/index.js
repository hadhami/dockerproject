var express =require('express');
const app = express();
const mongoose=require('mongoose');
const bodyParser= require('body-parser');
const auth = require('./api/authentification.js');
mongoose.Promise=global.Promise;

app.use(bodyParser.urlencoded({extended: false}));

mongoose.connect('mongodb://DockerProject_mymongo_1:27017/myDb',{ useNewUrlParser: true },(err) => {
    if (err){
        console.log('err');
    }else{
        console.log('Connected to mymongo');
    }
});

app.use(bodyParser.urlencoded({extended: false}));

app.use('/auth',auth);
app.get('/',(req,res) => {
    res.send('hello');
});
app.listen(3000, () =>{
    console.log('Server on 3000');
});
