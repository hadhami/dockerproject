const mongoose = require('mongoose');
mongoose.Promise=global.Promise;
const Schema = mongoose.Schema;

const publicationSchema= new Schema({
    addedOn: {type : String ,default :""},
    categoryname: {type : String ,default :""},
    description: {type : String ,default :""},
    company: {type : String ,default :""},
    email: {type : String ,default :""},
    expiredOn: {type : String ,default :""},
    Keyuser:{type: String ,default :""},
    nom:{type: String ,default :""},
    phonenb:{type: String ,default :""},
    prenom:{type: String ,default :""},
    status:{type: String ,default :""},
    title:{type: String, unique: true ,default :""},
    url:{type: String ,default :""},
  
});


module.exports= mongoose.model('Publication',publicationSchema);

