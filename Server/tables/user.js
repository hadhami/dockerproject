const mongoose = require('mongoose');
mongoose.Promise=global.Promise;
const Schema = mongoose.Schema;



const userSchema= new Schema({
    nom: {type : String},
    prenom: {type : String},
    email: {type : String, unique : true},
    company: {type : String},
    position: {type : String},
    sector: {type : String},
    phonenb:{type: String},
    adress:{type: String},
    proof:{type: String},
    password:{type: String},
    posts : {type: String}
        
    
});


module.exports= mongoose.model('User',userSchema);
