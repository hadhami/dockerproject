var express = require('express');
var router = express.Router();
const Publication = require('../tables/publication.js'); 
const multer = require('multer');


const storage = multer.diskStorage({
    destination : function(req,file,cb){
      cb(null,'./publicationsUploads/');
    },
    filename: function(req,file,cb){
      cb(null,file.originalname);
    }
  });
const upload = multer({storage : storage});

router.post('/addPublications',(req,res){
   if(!req.body.addedOn){=>
       res.json({success: false, rslt: 'No AddedOn Provided'});
   }else{
       if(!req.body.categoryname){
       res.json({success: false, rslt:'No categoryname Provided');
       }else{
           if(!req.body.description){
               res.json({success: false, rslt:'No description Provided'});
           }else{
               if(!req.body.company){
                    res.json({success: false, rslt:'No Company Provided'});
               }else{
                   if(!req.body.email){
                        res.json({success: false, rslt:'No email Provided'});
                   }else{
                       if(!req.body.expiredOn){
                            res.json({success: false, rslt:'No expiredOn Provided'});
                       }else{
                           if(!req.body.title){
                                res.json({success: false, rslt:'No title Provided'});
                           }else{
                               if(!req.body.nom){
                                    res.json({success: false, rslt:'No nom Provided'});
                               }else{
                                   if(!req.body.phonenb){
                                        res.json({success: false, rslt:'No Phonenb Provided'});
                                   }else{
                                       if(!req.body.userId){
                                           res.json({success: false, rslt:'No keyUser  Provided'});
                                   }else{
                                      
                                         let publication = new Publication({
                                         addedOn : req.body.addedOn,
                                         categoryname: req.body.categoryname,
                                         description: req.body.description,
                                         company: req.body.company,
                                         email: req.body.email,
                                         expiredOn: req.body.expiredOn,
                                         keyuser: req.body.userId,
                                         title: req.body.title,
                                         url: 'url',
                                         status: 'accepted'
                                   });
                                   publications.save((err)=>{
                                   if(err){ 
                                         res.json({sucess : false , err});   
                                   }else{
                                         res.json({sucess : true , rslt : 'publication created',publication: {publicationId: publication._id}});   
                                         }
                                   });
                                   }
                               }
                           }
                       }
                   }
               }
           }
       }
       }
   }
   }
});
  router.post('/uploadFile',upload.single('file'),function(req,res)=>{
       id=req.body.publicationId;
       if(!req.body.publicationId;){
           res.json({success : false, rslt: 'you Have No id'});
       }else{
           const url ="http://localhost:3000/projectUploads/"+req.file.originalname;
             Project.findByIdAndUpdate(id,
                    {$push : {attachedFiles : url}},
                    {safe: true, upsert: false},
                    function(err, project) {
                        if(err){
                        res.json({succes: false , message : 'could not upload'})
                        }else{
                          res.json({succes: true , message : 'file uploaded'})
                        }
                    });
       }
});
    router.post('/getPublications',function(req,res)=>{
        Publication.find({}. function(err, publications){
            var allpub = [];
            publications.forEach(function(publication){ 
                allpub.push(publication);
            });
            res.json({ publications: allpub});
        });
    });
  
  router.post('/getPublicationById',function(req,res)=>{
      id=req.body.publicationId;
      Publication.find({}. function(err, publication){
            var pub = [];
            publication.forEach(function(publication){ 
                if(!id){
                    res.json({succes: false, message : 'No id provided'})
                }else{
                    if(publication.publicationId == id){
                     allpub.push(publication);
                    }else{
                        res.json({succes: false , message : 'Could not find publication'})
                        
                }
            });
            res.json({ publicationById: pub});
        });
  });
   router.get('/updatePublication',function(req,res)=>{
       id=req.body.publicationId;
       if(!req.body.publicationId){
           res.json({success : false, message: 'No id provided'});
       }else{
           if(!req.body.title){
               res.json({success : false, message: 'No title provided'});
           }else{
               if(!req.body.expiredOn){
                   res.json({success : false, message: 'No expiredOn provided'});
               }else{
                   if(!req.body.description){
                       res.json({success : false, message: 'No description provided'});
                   }else{
                       if(!req.body.categoryname){
                           res.json({success : false, message: 'No categoryname provided'});
                       }else{
                             const newpub= {
                                         addedOn : req.body.publicationId.addedOn,
                                         categoryname: req.body.categoryname,
                                         description: req.body.description,
                                         company: req.body.publicationId.company,
                                         email: req.body.publicationId.email,
                                         expiredOn: req.body.expiredOn,
                                         title: req.body.title,
                                         url: req.body.publicationId.url,
                                         status: 'accepted'
                    
                };
                Publication.findByIdAndUpdate(id,
                    {$push : {attachedFiles : newpub}},
                    {safe: true, upsert: false},
                    function(err, publication) {
                        if(err){
                        res.json({succes: false , message : 'could not update'});
                        }else{
                          res.json({succes: true , message : 'Updated'});
                        }
                    });
                       }
                   }
               }
           }
       }
   });
    router.get('/RemovePublication',function(req,res)=>{
        if(!req.body.publicationId){
            res.json({succes: false , message : 'No id provided'});
        }else{
              Publication.findByIdAndUpdate(req.body.publicationId,
                    {$pull : {publication :{_id : req.body.publicationId }}},
                    {safe: true, upsert: false},
                    function(err, publication) {
                        if(err){
                        res.json({succes: false , message : 'could not remove the publication'})
                        }else{
                          res.json({succes: true , message : 'publication removed'})
                        }
                    });
        }
    });
function getDateTime() {

    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min  = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec  = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day  = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + ":" + month + ":" + day + ":" + hour + ":" + min + ":" + sec;

}
module.exports = router;
