import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './home/header/header.component';
import { ContentComponent } from './home/content/content.component';
import { FooterComponent } from './home/footer/footer.component';
import {RouterModule, Routes} from "@angular/router";
import { ErrorComponent } from './error/error.component';
import { AboutComponent } from './home/about/about.component';
import { TestimonialComponent } from './home/testimonial/testimonial.component';
import { BlogComponent } from './home/blog/blog.component';
import {OwrServicesComponent} from "./home/owr-services/owr-services.component";
import { ContactComponent } from './home/contact/contact.component';
import {AuthGuard} from "./services/auth.guard";
import { BlogdetailComponent } from './home/blogdetail/blogdetail.component';
import { FormComponent } from './home/form/form.component';
import { SignInComponent } from './home/sign-in/sign-in.component';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import {RestService} from "./services/rest.service";
import { UploadComponent } from './home/upload/upload.component';
import { RedirectComponent } from './home/redirect/redirect.component';
import { PubPipe } from './home/blog/pub.pipe';
import {EventsModule} from "angular4-events";
import {NgxPaginationModule} from "ngx-pagination";










const routes: Routes = [
  {path:'', component: HomeComponent,canActivateChild:[AuthGuard],
  children:[
    {path:'', component: ContentComponent},
    {path:'about', component: AboutComponent},
    {path:'contact', component: ContactComponent},
    {path:'testimonial', component: TestimonialComponent},
    {path:'services', component: OwrServicesComponent},
    {path:'blog', component: BlogComponent},
    {path:'blogdetail/:id', component: BlogdetailComponent},
    {path:'form', component: FormComponent},
    {path:'sign', component: SignInComponent},
    {path:'upload', component: UploadComponent},
    {path:'redirect', component: RedirectComponent}






  ]},

  {path:'**', component: ErrorComponent}
]



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    ContentComponent,
    FooterComponent,
    ErrorComponent,
    AboutComponent,
    TestimonialComponent,
    BlogComponent,
    ContactComponent,
    OwrServicesComponent,
    BlogdetailComponent,
    FormComponent,
    SignInComponent,
    UploadComponent,
    RedirectComponent,
    PubPipe,





  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule,
    EventsModule.forRoot(),
    NgxPaginationModule

  ],
  providers: [AuthGuard,RestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
