import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivateChild {
  constructor(public router: Router){

  }
  canActivateChild(){
    if(localStorage.getItem("state")==="1"){
      return true;
    }
    this.router.navigate([""]);
    return false;
  }
}
