import { Component, OnInit } from '@angular/core';
import {RestService} from "../../services/rest.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
  comments: any;
  nbComments: any;
  messages: any;
  nbMessages: any;
  publications: any;
  nbPublications: any;
  users: any;
  nbUsers: any;
  constructor(public rest: RestService,public router:Router) {
    this.getAllComments();
    this.getAllMessage();
    this.getAllPublications();
    this.getAllUser();

  }
  getAllComments() {

  }
  getAllMessage(){

  }
  getAllPublications(){

  }
  getAllUser() {

  }
  ngOnInit() {
  }

}
