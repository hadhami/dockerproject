import { Component, OnInit , OnDestroy } from '@angular/core';
import {RestService} from "../../services/rest.service";
import {Router} from '@angular/router';
import {EventsService} from "../../services/events.service";
declare var require: any;
const swal = require('sweetalert2');
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit , OnDestroy  {

  contact: any;
  ophone: any
  oemail: any;
  oweb: any;
  oadress: any;
state:boolean=false;
closesub:any ;
  constructor(public rest: RestService, public  router: Router ,private pubsub: EventsService,private events: EventsService) {
    this.getAllContact();
    //console.log(localStorage.getItem("state"))

    this.closesub = this.pubsub.subscribe('state').subscribe((from) => {
      this.state = true;
    });
  }
  logOut() {


  }
  getAllContact(){

  }
  ngOnInit() {
  }
  ngOnDestroy() {
    this.closesub.unsubscribe();

  }
}



