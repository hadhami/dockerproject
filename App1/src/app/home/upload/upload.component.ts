import { Component, OnInit } from '@angular/core';
import {RestService} from "../../services/rest.service";
import {AngularFireDatabase} from "angularfire2/database";
import {FileUpload} from "../../file-upload";

declare var require: any;
const swal = require('sweetalert2');
@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
category: any;
categoryname: any;
company: any;
nom:any;
prenom: any;
email: any;
phonenb:any;
title:any;
expiredOn: any;
addedOn:any;
file: any;
description:any;
state: any;

  private basePath = '/publications';
  selectedFiles: FileList;
  currentFileUpload: FileUpload;
  progress: { percentage: number } = { percentage: 0 };

  constructor(public rest: RestService,private db: AngularFireDatabase) {
    this.getAllCategory();

  }

  getAllCategory() {

  }

  call(){

    let timerInterval
    swal({
      title: 'Please Sign In First!',
      html: 'I will close in <strong></strong> seconds.',
      timer: 2000,
      onOpen: () => {
        swal.showLoading()
        timerInterval = setInterval(() => {
          swal.getContent().querySelector('strong')
            .textContent = swal.getTimerLeft()
        }, 100)
      },
      onClose: () => {
        clearInterval(timerInterval)
      }
    }).then((result) => {
      if (
        // Read more about handling dismissals
      result.dismiss === swal.DismissReason.timer
      ) {
        console.log('I was closed by the timer')
      }
    })



    this.category='';
    this.categoryname='';
    this.company='';
    this.nom='';
    this.prenom='';
    this.email='';
    this.phonenb='';
    this.title='';
    this.expiredOn='';
    this.addedOn='';
    this.file='';
    this.description='';
  }


  selectFile(event) {
    const file = event.target.files.item(0);

    if (file.type.match('image.*')) {
      this.selectedFiles = event.target.files;
    } else {
      alert('invalid format!');
    }
  }










  ngOnInit() {
  }

}
