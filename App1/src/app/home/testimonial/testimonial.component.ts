import { Component, OnInit } from '@angular/core';
import {RestService} from "../../services/rest.service";
import {ActivatedRoute, Router} from "@angular/router";
import {NgxPaginationModule} from "ngx-pagination";
import {AngularFireAuth} from "angularfire2/auth";
import {catchError} from "rxjs/internal/operators";
import {EventsService} from "../../services/events.service";
declare var require: any;
const swal = require('sweetalert2');
@Component({
  selector: 'app-testimonial',
  templateUrl: './testimonial.component.html',
  styleUrls: ['./testimonial.component.css']
})
export class TestimonialComponent implements OnInit {
company: any;
position: any;
nom:any;
prenom: any;
comment: any;
comments: any;
userKey: any;
state: boolean;
  logo: any;
  url: any;
  collections=[];closesub;
constructor(public rest: RestService, public router: Router,public  auth : AngularFireAuth,private pubsub: EventsService) {
  this.getAllComments();
  this.getAllLogo();

  this.closesub = this.pubsub.subscribe('state').subscribe((from) => {
    this.state = true;
  });

//console.log(this.auth.auth.currentUser.uid)
  if(this.auth.auth.currentUser.uid !== undefined) {



   // console.log(this.auth.auth.currentUser.uid);

  }
}
getAllComments(){

}
addComment(nom, prenom, company, position, comment){
  if(this.nom==undefined|| this.prenom==undefined||this.company==undefined||this.comment==undefined){
    swal({
      type: 'error',
      title: 'Oops...',
      text: 'Please Sign In First!' +
      ' Try again ',

    })

  }

  //this.rest.addComment(nom, prenom, company, position, comment)
  swal(
    'Good job!',
    'Added successfully',
    'success'
  )
  console.log(nom, prenom, company, position, comment)
  this.nom="";
  this.prenom="";
  this.company="";
  this.position="";
  this.comment="";

}
getAllLogo(){

  }
delet(key) {
    swal({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {




        console.log('okkkk'+key);
        /*this.rest.removeComment(key).then(res=>{
          console.log('remove');

          swal(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          );
        });*/

      }
    });

    //this.getAllComments();

  }

  ngOnInit() {

  }
}
