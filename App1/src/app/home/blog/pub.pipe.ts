import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pub'
})
export class PubPipe implements PipeTransform {

  transform(value: any,term:string): any {
    if (term===undefined){
      return value;
    }
    return value.filter(item=>
      item.payload.val().categoryname.includes(term)
    );
  }

}
