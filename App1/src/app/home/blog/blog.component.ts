import {Component, OnDestroy, OnInit} from '@angular/core';
import {RestService} from "../../services/rest.service";
import {ActivatedRoute} from "@angular/router";
import { Location } from '@angular/common';
import {EventsService} from "../../services/events.service";
import {AngularFireAuth} from "angularfire2/auth";
import {assertNumber} from "@angular/core/src/render3/assert";
declare var require: any;
const swal = require('sweetalert2');
@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit, OnDestroy {
publications: any;
addedOn: any;
expiredOn: any;
title: any;
file: any;
description: any;
company:any;
email: any;
categoryname: any;
phonenb: any;
  closesub:any ;
  userKey: any ;
  category: any;
key: any;
collections= [];


  constructor(public rest: RestService, private route: ActivatedRoute,  private location: Location ,private pubsub: EventsService , public  auth : AngularFireAuth) {


    this.userKey=this.auth.auth.currentUser.uid

//     this.pubsub.subscribe('user').subscribe((from) => {
//       console.log("done")
//       console.log(from)
//       this.userKey=from
//       console.log(this.userKey)
//     });
//    // return this.closesub;
// console.log(this.userKey)
    this.getAllPublications();
    this.getAllCategory();

//console.log("date:"+this.rest.getCurrentDate());
  }
  getAllPublications(){

  }

  deleteFileUpload(key) {
    swal({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {




        console.log('okkkk'+key);
        /*this.rest.removePublications(key).then(res=>{
          console.log('remove');

          swal(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          );
        });*/

      }
    });

    this.getAllPublications();

  }
  update(key,title,expiredOn,description, categoryname){

  }
  upDatePublication(key,title,expiredOn,description, categoryname){

    swal(
      'Good job!',
      'Updated successfully',
      'success'
    )

  }
  getAllCategory() {

  }

  ngOnInit() {

  }
  ngOnDestroy() {
    //this.closesub.unsubscribe();

  }
}
